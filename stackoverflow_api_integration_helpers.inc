<?php
/**
 * @file
 * Stackoverflow helpers.
 */

/**
 * Returns the time elapsed as an string thx zachstronaut.
 */
function _stackoverflow_api_integration_time_elapsed_string($ptime) {
  $etime = time() - $ptime;

  if ($etime < 1) {
    return '0 seconds';
  }

  $a = array(
    12 * 30 * 24 * 60 * 60  => 'year',
    30 * 24 * 60 * 60       => 'month',
    24 * 60 * 60            => 'day',
    60 * 60                 => 'hour',
    60                      => 'minute',
    1                       => 'second',
  );

  foreach ($a as $secs => $str) {
    $d = $etime / $secs;
    if ($d >= 1) {
      $r = round($d);
      return $r . ' ' . $str . ($r > 1 ? 's' : '');
    }
  }
}

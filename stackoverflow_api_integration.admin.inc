<?php
/**
 * @file
 * Stackoverflow main logic.
 */

/**
 * Builds a form with various settigns for stackoverflow.
 */
function stackoverflow_api_integration_settings() {
  // Fieldsets.
  $form['stackoverflow_api_integration_standard'] = array(
    '#title' => t('Search settings'),
    '#type' => 'fieldset',
    '#collapsible' => TRUE,
    '#collapsed' => FALSE,
  );
  $form['stackoverflow_api_integration_navigation'] = array(
    '#title' => t('Navigation settings'),
    '#type' => 'fieldset',
    '#collapsible' => TRUE,
    '#collapsed' => TRUE,
  );
  $form['stackoverflow_api_integration_search'] = array(
    '#title' => t('Search settings'),
    '#type' => 'fieldset',
    '#collapsible' => TRUE,
    '#collapsed' => TRUE,
  );
  // Enable navigation.
  $form['stackoverflow_api_integration_navigation']['stackoverflow_api_integration_navigation'] = array(
    '#type' => 'checkbox',
    '#title' => t('Enable pagination.'),
    '#default_value' => variable_get('stackoverflow_api_integration_navigation', ''),
  );
  // Double navigation.
  $form['stackoverflow_api_integration_navigation']['stackoverflow_api_integration_double_navigation'] = array(
    '#type' => 'checkbox',
    '#title' => t('Enable double pagination.'),
    '#default_value' => variable_get('stackoverflow_api_integration_double_navigation', ''),
  );
  // Items per page navigation.
  $form['stackoverflow_api_integration_navigation']['stackoverflow_api_integration_navigation_items_per_page'] = array(
    '#type' => 'select',
    '#title' => t('Items per page.'),
    '#options' => array(
      '1' => '1',
      '2' => '2',
      '5' => '5',
      '10' => '10',
      '10' => '15',
      '20' => '20',
      '25' => '25',
      '30' => '30',
      '40' => '40',
      '50' => '50',
      '60' => '60',
    ),
    '#default_value' => variable_get('stackoverflow_api_integration_navigation_items_per_page', '5'),
  );
  // Label First.
  $form['stackoverflow_api_integration_navigation']['stackoverflow_api_integration_navigation_label_first'] = array(
    '#type' => 'textfield',
    '#title' => t('Label first.'),
    '#default_value' => variable_get('stackoverflow_api_integration_navigation_label_first', '<<'),
  );
  // Label previous.
  $form['stackoverflow_api_integration_navigation']['stackoverflow_api_integration_navigation_label_previous'] = array(
    '#type' => 'textfield',
    '#title' => t('Label previous.'),
    '#default_value' => variable_get('stackoverflow_api_integration_navigation_label_previous', '<'),
  );
  // Label next.
  $form['stackoverflow_api_integration_navigation']['stackoverflow_api_integration_navigation_label_next'] = array(
    '#type' => 'textfield',
    '#title' => t('Label next.'),
    '#default_value' => variable_get('stackoverflow_api_integration_navigation_label_next', '>'),
  );
  // Label Last.
  $form['stackoverflow_api_integration_navigation']['stackoverflow_api_integration_navigation_label_last'] = array(
    '#type' => 'textfield',
    '#title' => t('Label last.'),
    '#default_value' => variable_get('stackoverflow_api_integration_navigation_label_last', '>>'),
  );
  // Theme navigation.
  $form['stackoverflow_api_integration_navigation']['stackoverflow_api_integration_navigation_theme_pagination'] = array(
    '#type' => 'checkbox',
    '#title' => t('Theme navigation list?'),
    '#default_value' => variable_get('stackoverflow_api_integration_navigation_theme_pagination', 1),
  );
  // Theme navigation.
  $form['stackoverflow_api_integration_navigation']['stackoverflow_api_integration_navigation_theme_pagination_color'] = array(
    '#type' => 'textfield',
    '#title' => t('HTML Color code of the color of the odd rows'),
    '#default_value' => variable_get('stackoverflow_api_integration_navigation_theme_pagination_color', 'yellow'),
  );
  // Body trim limit.
  $form['stackoverflow_api_integration_standard']['stackoverflow_api_integration_body_trim_limit'] = array(
    '#type' => 'textfield',
    '#title' => t('Body trim length'),
    '#default_value' => variable_get('stackoverflow_api_integration_body_trim_limit', 300),
  );
  $form['stackoverflow_api_integration_standard']['stackoverflow_api_integration_cache_expiration'] = array(
    '#type' => 'textfield',
    '#title' => t('Cache expiration.'),
    '#default_value' => variable_get('stackoverflow_api_integration_cache_expiration', 3600),
  );
  // Read More links.
  $form['stackoverflow_api_integration_standard']['stackoverflow_api_integration_read_more_link'] = array(
    '#type' => 'checkbox',
    '#title' => t('Do you want a read more link?'),
    '#default_value' => variable_get('stackoverflow_api_integration_read_more_link', ''),
  );
  // Read More links in drupal ?.
  $form['stackoverflow_api_integration_standard']['stackoverflow_api_integration_read_more_link_drupal'] = array(
    '#type' => 'checkbox',
    '#title' => t('Read more link and title stay in website(Drupal)?'),
    '#default_value' => variable_get('stackoverflow_api_integration_read_more_link_drupal', ''),
  );
  // Search order.
  $form['stackoverflow_api_integration_standard']['stackoverflow_api_integration_search_order'] = array(
    '#type' => 'select',
    '#title' => t('Search order.'),
    '#options' => array(
      'desc' => 'Descending',
      'asc' => 'Ascending',
    ),
    '#default_value' => variable_get('stackoverflow_api_integration_search_order', 'desc'),
  );
  // Search sort.
  $form['stackoverflow_api_integration_standard']['stackoverflow_api_integration_search_sort'] = array(
    '#type' => 'select',
    '#title' => t('Search sort.'),
    '#options' => array(
      'activity' => 'Activity',
      'votes' => 'Votes',
      'creation' => 'creation',
      'relevance' => 'Relevance',
    ),
    '#default_value' => variable_get('stackoverflow_api_integration_search_sort', 'activity'),
  );
  // Search filter.
  $form['stackoverflow_api_integration_standard']['stackoverflow_api_integration_search_filter'] = array(
    '#type' => 'select',
    '#title' => t('Search Filter.'),
    '#options' => array(
      'default' => 'Default',
    ),
    '#default_value' => variable_get('stackoverflow_api_integration_search_filter', 'default'),
  );
  // Search site.
  $form['stackoverflow_api_integration_standard']['stackoverflow_api_integration_search_site'] = array(
    '#type' => 'select',
    '#title' => t('Search Site.'),
    '#options' => array(
      'stackoverflow' => 'stackoverflow',
    ),
    '#default_value' => variable_get('stackoverflow_api_integration_search_site', 'stackoverflow'),
  );
  // Form which date.
  $form['stackoverflow_api_integration_search']['stackoverflow_api_integration_from_date'] = array(
    '#type' => 'textfield',
    '#title' => t('From Date.'),
    '#default_value' => variable_get('stackoverflow_api_integration_from_date', ''),
  );
  // To which date.
  $form['stackoverflow_api_integration_search']['stackoverflow_api_integration_to_date'] = array(
    '#type' => 'textfield',
    '#title' => t('To Date.'),
    '#default_value' => variable_get('stackoverflow_api_integration_to_date', ''),
  );
  $form['stackoverflow_api_integration_search']['stackoverflow_api_integration_no_result_msg'] = array(
    '#type' => 'textfield',
    '#title' => t('No results message for the users.'),
    '#default_value' => variable_get('stackoverflow_api_integration_no_result_msg', ''),
  );
  return system_settings_form($form);
}

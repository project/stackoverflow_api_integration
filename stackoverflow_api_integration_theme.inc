<?php
/**
 * @file
 * Stackoverflow theming functions.
 */

/**
 * Theme an item list of questions, answers etc.
 *
 * The result @param array $result.
 * Title of list @param string $title
 * Type of thming @param string $type_of_theming
 * An extra type for theming @param string $theming_param
 * How many items for nav @param integer $items_per_page_navigation
 * Want double nav @param string $double_navigtion
 * Want pagination themed @param string $theme_pagination
 * return an html list @return string
 */
function _stackoverflow_api_integration_theme_item_list($result, $title = 'Stackoverflow',
    $type_of_theming = 'search', $theming_param = 'default', $items_per_page_navigation = NULL, $double_navigtion = NULL, $theme_pagination = NULL, $body_trim = TRUE) {
  // Get variables.
  if (empty($double_pagination)) {
    $double_pagination = variable_get('stackoverflow_api_integration_double_navigation', '');
  }
  if (empty($items_per_page_navigation)) {
    $items_per_page_navigation = variable_get('stackoverflow_api_integration_navigation_items_per_page', '5');
  }
  // Need to do this.
  if (empty($theme_pagination)) {
    $theme_pagination = variable_get('stackoverflow_api_integration_navigation_theme_pagination', 'true');
  }
  // Body trimming.
  if ($body_trim == FALSE) {
    $body_trim_limit = variable_get('stackoverflow_api_integration_body_trim_limit', '300');
  }
  $need_read_more_link = variable_get('stackoverflow_api_integration_read_more_link', 'TRUE');
  $need_read_more_and_title_stay_in_drupal = variable_get('stackoverflow_api_integration_read_more_link_drupal', 'TRUE');
  // Add navigation if requested.
  $pagination_enabled = variable_get('stackoverflow_api_integration_navigation', '');
  // Sanitize first.
  $title = check_plain($title);
  $type_of_theming = check_plain($type_of_theming);
  $theming_param = check_plain($theming_param);

  // Type of list.
  $type = 'ul';
  // The following attributes apply to the list tag (e.g., <ol> or <ul>).
  $attributes = array(
    'id' => 'stackoverflow-api-listing',
    'class' => 'stackoverflow-listing ' . $title,
  );

  $items = _stackoverflow_api_integration_theme_create_themed_items($result, $items_per_page_navigation,
      $theming_param, $type_of_theming, $body_trim_limit, $need_read_more_link, $need_read_more_and_title_stay_in_drupal, $theme_pagination);

  // Drupal theme the item list.
  $themed_item_list = theme_item_list(array(
    'items' => $items,
    'title' => $title,
    'type' => $type,
    'attributes' => $attributes,
  ));
  // When empty provide default msg.
  if (empty($items)) {
    $themed_item_list = variable_get('stackoverflow_api_integration_no_result_msg', t('There are no items for your search query.'));
  }
  // Now really add the pagination.
  if ($pagination_enabled) {
    return _stackoverflow_api_integration_theme_pagination_item_list($themed_item_list, $double_pagination, $theme_pagination);
  }
  else {
    return $themed_item_list;
  }
}

/**
 * Create an themed item list.
 *
 * The result that needs theming @param array $result
 * How many items per nav @param integer $items_per_page_navigation
 * Theming param @param string $theming_param
 * Extra theming param @param string $type_of_theming
 * Themed_item_list_with_nav @return html_list
 */
function _stackoverflow_api_integration_theme_create_themed_items($result, $items_per_page_navigation, $theming_param,
    $type_of_theming, $body_trim_limit, $need_read_more_link, $need_read_more_and_title_stay_in_drupal, $theme_pagination) {
  // Logic navigation.
  $max_count = count($result->items);
  $page = pager_default_initialize($max_count, $items_per_page_navigation);
  // Only theme items for the current page.
  // Remove items that aren't current.
  // If not on the 0 zero page.
  if (!empty($page)) {
    $current_pages_that_can_be_outputted = ($items_per_page_navigation * $page);
  }
  else {
    $current_pages_that_can_be_outputted = $items_per_page_navigation;
  }
  $items = array();
  $current_item_counter = 0;
  foreach ($result->items as $result_item) {
    if ($current_item_counter < $current_pages_that_can_be_outputted
        && ($current_item_counter >= ($current_pages_that_can_be_outputted - $items_per_page_navigation))) {
      $body = isset($body_trim_limit) ? text_summary($result_item->body, NULL, $body_trim_limit) : $result_item->body;
      $link = ($need_read_more_and_title_stay_in_drupal) ? 'stackoverflow/questions_answers/' . $result_item->question_id : $result_item->link;
      if ($need_read_more_and_title_stay_in_drupal) {
        $link_to_item = l($result_item->title, $link);
      }
      else {
        $link_to_item = l($result_item->title, $link, array('attributes' => array('target' => '_blank')));
      }
      $short_data = $link_to_item;
      $long_data = $link_to_item . '<span class="date">'
        . _stackoverflow_api_integration_time_elapsed_string($result_item->creation_date) . ' ' . t('ago')
        . '</span><p class="stackoverflow-body-answer">' . $body . '</p>';
      $long_data .= isset($need_read_more_link) ? '<span class="stackoverflow-read-more">' . l(t('Read more'), $link) . '</span>' : '';
      $odd_or_even = ($current_item_counter % 2) ? ' odd' : ' even';
      $items[] = array(
        'data' => ($theming_param == 'default') ? $long_data : $short_data,
        'id' => $result_item->question_id,
        'class' => array('class' => 'stackoverflow-api-' . $type_of_theming . $odd_or_even),
      );
    }
    // Do +1 for the counter.
    $current_item_counter++;
  }
  return $items;
}

/**
 * Themed pagination item list.
 *
 * Themed_item_list @param html_list $themed_item_list
 */
function _stackoverflow_api_integration_theme_pagination_item_list($themed_item_list, $double_pagination, $theme_pagination) {
  // Thmed pager based upon settings.
  $nav_label_first = variable_get('stackoverflow_api_integration_navigation_label_first', '<<');
  $nav_label_prev = variable_get('stackoverflow_api_integration_navigation_label_previous', '<');
  $nav_label_next = variable_get('stackoverflow_api_integration_navigation_label_next', '>');
  $nav_label_last = variable_get('stackoverflow_api_integration_navigation_label_last', '>>');
  $themed_pager = theme(
    'pager', array(
      'tags' => array(
        $nav_label_first,
        $nav_label_prev,
        '',
        $nav_label_next, $nav_label_last,
      ),
    )
  );
  // Container div.
  $final_result = '<div id="stackoverflow-api-integration-pagination">';
  // Add double navigation of requested.
  if ($double_pagination) {
    $final_result .= '<div class="page_navigation">' . $themed_pager . '</div>';
  }
  // Add the result.
  $final_result .= $themed_item_list;
  // Navigation.
  $final_result .= '<div class="page_navigation">' . $themed_pager . '</div>';
  // End container div.
  $final_result .= '</div>';
  if ($theme_pagination) {
    $final_result .= '<style type="text/css">#stackoverflow-api-listing li.odd {  background-color: ' . variable_get('stackoverflow_api_integration_navigation_theme_pagination_color', 'grey') . ';}</style>';
  }
  return $final_result;
}
